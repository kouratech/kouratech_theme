{
    'name': 'Thème kouratech 2.0',
    'sequence': 0,
    'description': 'Thème pour koura',
    'version': '2.0',
    'author': 'KOURATECH',

    'data': [
        'views/layout.xml',
        'views/assets.xml',
        'views/snippets.xml',
        #'views/pages.xml',
    ],
    'images': ['static/description/logo.png', 'static/description/logo.png'],
    'category': 'Theme/itech',
    'depends': ['website'],
    'application':True,
    'sequence':'1'
}
